# AsanaSync

Local Syncing of Asana (Assets) for faster database queries than Asana's API can provide without exceeding Asana's limits.

# Database

Requires an 'asana' database connection defined.
