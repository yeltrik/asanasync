<?php

use Illuminate\Support\Facades\Route;
use Yeltrik\AsanaSync\app\Http\controllers\AsanaController;
use Yeltrik\AsanaSync\app\Http\controllers\AssigneeController;
use Yeltrik\AsanaSync\app\Http\controllers\AttachmentController;
use Yeltrik\AsanaSync\app\Http\controllers\ProjectController;
use Yeltrik\AsanaSync\app\Http\controllers\ProjectCustomFieldController;
use Yeltrik\AsanaSync\app\Http\controllers\ProjectNameNoteController;
use Yeltrik\AsanaSync\app\Http\controllers\ProjectTaskAssigneeController;
use Yeltrik\AsanaSync\app\Http\controllers\ProjectTaskController;
use Yeltrik\AsanaSync\app\Http\controllers\ProjectTaskCustomFieldController;
use Yeltrik\AsanaSync\app\Http\controllers\ProjectTaskModifiedController;
use Yeltrik\AsanaSync\app\Http\controllers\ProjectTaskParentController;
use Yeltrik\AsanaSync\app\Http\controllers\TaskController;

Route::group(['middleware' => 'web'], function () {

    Route::get('asana', [AsanaController::class, 'index'])->name('asana.index');

    Route::get('asana/assignee/', [AssigneeController::class, 'index'])->name('asana.assignees.index');
    Route::get('asana/assignee/{assignee}', [AssigneeController::class, 'show'])->name('asana.assignees.show');

    Route::get('asana/attachment/', [AttachmentController::class, 'index'])->name('asana.attachments.index');
    Route::get('asana/attachment/{attachment}', [AttachmentController::class, 'show'])->name('asana.attachments.show');

    //Route::delete('asana/task/{task}', [TaskController::class, 'destroy'])->name('asana.tasks.destroy');
    Route::get('asana/task', [TaskController::class, 'index'])->name('asana.tasks.index');
    //Route::get('asana/task/create', [TaskController::class, 'create'])->name('asana.tasks.create');
    Route::get('asana/task/{task}', [TaskController::class, 'show'])->name('asana.tasks.show');
    //Route::get('asana/task/{task}/edit', [TaskController::class, 'edit'])->name('asana.tasks.edit');
    Route::get('asana/task/{task}/sync', [TaskController::class, 'sync'])->name('asana.tasks.sync');
    //Route::patch('asana/task/{task}', [TaskController::class, 'update'])->name('asana.tasks.update');
    //Route::post('asana/task', [TaskController::class, 'store'])->name('asana.tasks.store');

    //Route::delete('asana/project/{project}', [ProjectController::class, 'destroy'])->name('asana.projects.destroy');
    Route::get('asana/project', [ProjectController::class, 'index'])->name('asana.projects.index');
    //Route::get('asana/project/{project}/attachments', [ProjectController::class, 'attachments'])->name('asana.projects.attachments.index');
    Route::get('asana/project/create', [ProjectController::class, 'create'])->name('asana.projects.create');
    Route::get('asana/project/{project}', [ProjectController::class, 'show'])->name('asana.projects.show');
    //Route::get('asana/project/{project}/edit', [ProjectController::class, 'edit'])->name('asana.projects.edit');
    Route::get('asana/project/{project}/custom-field/sync', [ProjectCustomFieldController::class, 'sync'])->name('asana.projects.custom-fields.sync');
    Route::get('asana/project/{project}/name-note/sync', [ProjectNameNoteController::class, 'sync'])->name('asana.projects.name-note.sync');
    Route::get('asana/project/{project}/sync', [ProjectController::class, 'sync'])->name('asana.projects.sync');
    Route::get('asana/project/{project}/task/assignee/sync', [ProjectTaskAssigneeController::class, 'sync'])->name('asana.projects.tasks.assignees.sync');
    Route::get('asana/project/{project}/task/custom-field/sync', [ProjectTaskCustomFieldController::class, 'sync'])->name('asana.projects.tasks.custom-fields.sync');
    Route::get('asana/project/{project}/task/modified/sync', [ProjectTaskModifiedController::class, 'sync'])->name('asana.projects.tasks.modified.sync');
    Route::get('asana/project/{project}/task/parent/sync', [ProjectTaskParentController::class, 'sync'])->name('asana.projects.tasks.parent.sync');
    Route::get('asana/project/{project}/task/sync', [ProjectTaskController::class, 'sync'])->name('asana.projects.tasks.sync');

    //Route::patch('asana/project/{project}', [ProjectController::class, 'update'])->name('asana.projects.update');
    Route::post('asana/project', [ProjectController::class, 'store'])->name('asana.projects.store');

});
