<?php

namespace Yeltrik\AsanaSync\app;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Assignee
 * @property string id
 * @property string name
 * @property string email
 * @package Yeltrik\AsanaSync\app
 */
class Assignee extends Model
{

    protected $connection = 'asana';
    public $table = 'assignees';

    /**
     * Assignee constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->table = env('DB_DATABASE_ASANA', $this->connection) . '.' . $this->table;
        parent::__construct($attributes);
    }

    /**
     * @return HasMany
     */
    public function tasks()
    {
        return $this->hasMany(Task::class);
    }

    /**
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'email', 'email');
    }

}
