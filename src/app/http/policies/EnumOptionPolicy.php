<?php

namespace Yeltrik\AsanaSync\app\Http\policies;

use Yeltrik\AsanaSync\app\EnumOption;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class EnumOptionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param User $user
     * @param EnumOption $enumOption
     * @return mixed
     */
    public function view(User $user, EnumOption $enumOption)
    {
        //
    }

}
