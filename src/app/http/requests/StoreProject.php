<?php

namespace Yeltrik\AsanaSync\app\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

/**
 * Class StoreProject
 * @property string id
 * @property string name
 * @package Yeltrik\AsanaSync\app\Http\Requests
 */
class StoreProject extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    public function messages()
    {
        return [
            'id.required' => "An Asana GID is Required",
            'id.unique' => "This Asana Project has already added",
            'id.min' => "An Asana GID is a number 16 digits long",
            'id.max' => "An Asana GID is a number 16 digits long",
            'name.required'  => 'Please Enter the Project Name',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|unique:projects|min:16|max:16|',
            'name' => 'required',
        ];
    }
}
