<?php

namespace Yeltrik\AsanaSync\app\Http\controllers;

use App\Http\Controllers\Controller;
use Asana\Client;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Yeltrik\AsanaSync\app\Project;

class ProjectTaskCustomFieldController extends Controller
{

    /**
     * @param  Project  $project
     * @return RedirectResponse
     */
    public function sync(Project $project)
    {
        ini_set('max_execution_time', 300);

        if (Auth::check()) {
            $asanaClient = Client::accessToken(env('ASANA_PERSONAL_ACCESS_TOKEN'));

            $limit = 2;

            $asanaTasks = $asanaClient->tasks->getTasksForProject( $project->id, [], [
                'limit' => $limit,
                'opt_fields' => implode(", ", [
                    'name',
                    'resource_type',
                    'projects', 'projects.name',
                    'custom_fields',
                ])
            ]);

            //dd($asanaTasks);

            $asanaTaskArray = iterator_to_array($asanaTasks);

            foreach ($asanaTaskArray as $asanaTask) {
                $task = (new TaskController())->syncAsanaTask($asanaTask);

                foreach($asanaTask->projects as $asanaProject) {
                    if ( $asanaProject->gid == $project->id) {
                        if (!$task->projects()->find($project)) {
                            $task->projects()->attach($project);
                        }
                    }
                }

//                if ( sizeof($asanaTask->projects) == 0 ) {
//                    // Child Task that is not in this task
//                    //dd($asanaTask);
//                }
            }

            // If we got back the limit, then lets sync again
            if ( sizeof($asanaTaskArray) == $limit ) {
                return redirect()->route('asana.projects.tasks.custom-field.sync', [
                    'project' => $project,
                ]);
            } else {

                return redirect()->route('asana.projects.show', [
                    'project' => $project,
                ]);
            }
        } else {
            return redirect()->route('login');
        }
    }

}
