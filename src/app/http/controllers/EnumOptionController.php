<?php

namespace Yeltrik\AsanaSync\app\Http\controllers;

use Yeltrik\AsanaSync\app\EnumOption;
use App\Http\Controllers\Controller;

class EnumOptionController extends Controller
{

    /**
     * EnumOptionController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     *
     */
    public function index()
    {
        //
    }

    /**
     * @param EnumOption $enumOption
     */
    public function show(EnumOption $enumOption)
    {
        //
    }

}
