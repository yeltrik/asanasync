<?php

namespace Yeltrik\AsanaSync\app\Http\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Yeltrik\AsanaSync\app\Attachment;

class AttachmentController extends Controller
{

    /**
     * AttachmentController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|Response|View
     */
    public function index()
    {
        $attachments = Attachment::query()->paginate(10);
        return view('AsanaSync::attachment.index', compact('attachments'));
    }

    /**
     * Display the specified resource.
     *
     * @param Attachment $attachment
     * @return Application|Factory|Response|View
     */
    public function show(Attachment $attachment)
    {
        return view('AsanaSync::attachment.show', compact('attachment'));
    }

}
