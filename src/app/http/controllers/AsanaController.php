<?php

namespace Yeltrik\AsanaSync\app\Http\controllers;

use App\Http\Controllers\Controller;

class AsanaController extends Controller
{
    /**
     * AttachmentController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('AsanaSync::asana.index');
    }

}
