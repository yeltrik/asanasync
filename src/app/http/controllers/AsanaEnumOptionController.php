<?php

namespace Yeltrik\AsanaSync\app\Http\controllers;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\RedirectResponse;
use Yeltrik\AsanaSync\app\EnumOption;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AsanaEnumOptionController extends Controller
{

    /**
     * @param $asanaEnumOption
     * @return EnumOption|Builder|Builder[]|Collection|Model|RedirectResponse|null
     */
    public function sync($asanaEnumOption)
    {
        if(Auth::check()) {
            $enumOption = EnumOption::query()->find($asanaEnumOption->gid);
            if ($enumOption instanceof EnumOption === FALSE) {
                $enumOption = new EnumOption();
                $enumOption->id = $asanaEnumOption->gid;
            }

            // Updates?
            $enumOption->name = $asanaEnumOption->name;
            $enumOption->enabled = $asanaEnumOption->enabled;
            $enumOption->save();

            return $enumOption;
        } else {
            return redirect()->route('login');
        }
    }

}
