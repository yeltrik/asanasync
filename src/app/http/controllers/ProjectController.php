<?php

namespace Yeltrik\AsanaSync\app\Http\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Yeltrik\AsanaSync\app\Http\Requests\StoreProject;
use Yeltrik\AsanaSync\app\Project;
use Illuminate\Support\Facades\Auth;

class ProjectController extends Controller
{

    /**
     * ProjectController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return Application|Factory|RedirectResponse|View
     */
    public function index()
    {
        if ( Auth::check() ) {
            $projects = Project::all();
            return view('AsanaSync::project.index', compact('projects'));
        } else {
            return redirect()->route('welcome');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|RedirectResponse|Response|View
     */
    public function create()
    {
        if ( Auth::check() ) {
            return view('AsanaSync::project.create');
        } else {
            return redirect()->route('login');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreProject  $request
     * @return RedirectResponse|Response
     */
    public function store(StoreProject $request)
    {
        $asanaProjectController = new ProjectController();
        $project = new Project();
        $project->id = $request->id;
        $project->name = $request->name;
        $project->save();
        $asanaProjectController->sync($project);
        return redirect()->route('asana.projects.sync', $project);
    }

    /**
     * @param string $projectId
     * @return Application|Factory|RedirectResponse|View|null
     */
    public function show(string $projectId)
    {
        $project = Project::query()->find($projectId);
        if ( $project === NULL ) {
            //$storeProject = new StoreProject();
            //$storeProject->id = $projectId;
            //$storeProject->name = '{SYNC}';
            $project = new Project();
            $project->id = $projectId;
            $project->name = '{SYNC}';
            $project->save();
            return redirect()->route('asana.projects.sync', $project);
        }
        if( Auth::check()) {
            if( $project instanceof Project) {
                $tasks = $project->tasks()->paginate(10);
                return view('AsanaSync::project.show', compact('project', 'tasks'));
            }
        } else {
            return redirect()->route(   'login');
        }
        return null;
    }

    /**
     * @param  Project  $project
     * @return RedirectResponse
     */
    public function sync(Project $project)
    {
        (new ProjectNameNoteController())->sync($project);

        return redirect()->route('asana.projects.show', $project);
    }

}
