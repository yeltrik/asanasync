<?php

namespace Yeltrik\AsanaSync\app\Http\controllers;

use App\Http\Controllers\Controller;
use Asana\Client;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Yeltrik\AsanaSync\app\Project;

class ProjectNameNoteController extends Controller
{

    /**
     * @param  Project  $project
     * @return RedirectResponse
     */
    public function sync(Project $project)
    {
        if(Auth::check()) {
            $asanaClient = Client::accessToken(env('ASANA_PERSONAL_ACCESS_TOKEN'));
            $asanaProject = $asanaClient->projects->getProject($project->id);

            // Updates?
            $project->name = $asanaProject->name;
            $project->notes = $asanaProject->notes;
            $project->save();

            return redirect()->route('asana.projects.show', $project);
        } else {
            return redirect()->route('login');
        }
    }

}
