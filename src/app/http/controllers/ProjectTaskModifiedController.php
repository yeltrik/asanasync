<?php

namespace Yeltrik\AsanaSync\app\Http\controllers;

use App\Http\Controllers\Controller;
use Asana\Client;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Yeltrik\AsanaSync\app\Project;
use Yeltrik\AsanaSync\app\Task;

class ProjectTaskModifiedController extends Controller
{

    /**
     * @param  Project  $project
     * @return RedirectResponse
     */
    public function sync(Project $project)
    {
        ini_set('max_execution_time', 300);

        if (Auth::check()) {
            $asanaClient = Client::accessToken(env('ASANA_PERSONAL_ACCESS_TOKEN'));

            $limit = 100;
            $latestTask = $project->tasks()->orderBy('modified_at', 'desc')->first();
            if  ( $latestTask instanceof Task ) {
                $modifiedAtAfter = $latestTask->modified_at;
            } else {
                $modifiedAtAfter = NULL;
            }
            $asanaTasks = $asanaClient->tasks->searchTasksForWorkspace( '1114644022429341', [
                'projects.any' => $project->id,
                'sort_by' => 'modified_at',
                'sort_ascending' => TRUE,
                'modified_at.after' => $modifiedAtAfter,
                //is_subtask' => FALSE,
            ], [
                'iterator_type' => FALSE,
                'limit' => $limit,
                'opt_fields' => implode(", ", [
                    'name',
                    'resource_type',
                    'custom_fields',
                    'completed, completed_at',
                    'created_at, modified_at',
                    'attachments, attachments.name, attachments.created_at, attachments.download_url, attachments.view_url',
                    'assignee, assignee.name, assignee.email',
                    'parent, parent.name,',
                    'parent.projects, parent.projects.name',
                    'projects', 'projects.name',
                ])
            ]);

            //dd($asanaTasks->data);

            foreach ($asanaTasks->data as $asanaTask) {
                $task = (new TaskController())->syncAsanaTask($asanaTask);

                foreach($asanaTask->projects as $asanaProject) {
                    if ( $asanaProject->gid == $project->id) {
                        if (!$task->projects()->find($project)) {
                            $task->projects()->attach($project);
                        }
                    }
                }

//                if ( sizeof($asanaTask->projects) == 0 ) {
//                    // Child Task that is not in this task
//                    //dd($asanaTask);
//                }
            }

            // If we got back the limit, then lets sync again
            if ( sizeof($asanaTasks->data) == $limit ) {
                return redirect()->route('asana.projects.tasks.sync', [
                    'project' => $project,
                ]);
            } else {

                return redirect()->route('asana.projects.show', [
                    'project' => $project,
                ]);
            }
        } else {
            return redirect()->route('login');
        }
    }

}
