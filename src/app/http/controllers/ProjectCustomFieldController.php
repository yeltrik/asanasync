<?php

namespace Yeltrik\AsanaSync\app\Http\controllers;

use App\Http\Controllers\Controller;
use Asana\Client;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Yeltrik\AsanaSync\app\Project;

class ProjectCustomFieldController extends Controller
{

    /**
     * @param  Project  $project
     * @return RedirectResponse
     */
    public function sync(Project $project)
    {
        if(Auth::check()) {
            $asanaClient = Client::accessToken(env('ASANA_PERSONAL_ACCESS_TOKEN'));
            $asanaProject = $asanaClient->projects->getProject($project->id);

            foreach ($asanaProject->custom_field_settings as $asanaCustomFieldSetting) {
                $customField = (new AsanaCustomFieldController())->sync($asanaCustomFieldSetting->custom_field);

                if (!$project->customFields()->find($customField->id)) {
                    $project->customFields()->attach($customField);
                }
            }

            return redirect()->route('asana.projects.show', $project);
        } else {
            return redirect()->route('login');
        }
    }

}
