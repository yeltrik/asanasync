<?php

namespace Yeltrik\AsanaSync\app\Http\controllers;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Yeltrik\AsanaSync\app\Assignee;
use App\Http\Controllers\Controller;

class AssigneeController extends Controller
{
    /**
     * AttachmentController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|Response|View
     */
    public function index()
    {
        return view('AsanaSync::assignee.index', [
            'assignees' => Assignee::all()
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param Assignee $assignee
     * @return Application|Factory|Response|View
     */
    public function show(Assignee $assignee)
    {
        return view('AsanaSync::assignee.show', compact('assignee'));
    }

}
