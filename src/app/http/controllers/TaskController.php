<?php

namespace Yeltrik\AsanaSync\app\Http\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Yeltrik\AsanaSync\app\Assignee;
use Yeltrik\AsanaSync\app\Task;
use Asana\Client;
use Illuminate\Support\Facades\Auth;

class TaskController extends Controller
{

    /**
     * TaskController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|RedirectResponse|Response|View
     */
    public function index()
    {
        if( Auth::check() ) {
            $tasks = Task::query()->paginate(10);
            return view('AsanaSync::task.index', compact('tasks'));
        } else {
            return redirect()->route('login');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  Task  $task
     * @return Application|Factory|RedirectResponse|Response|View
     */
    public function show(Task $task)
    {
        if( Auth::check() ) {
            return view('AsanaSync::task.show', compact('task'));
        } else {
            return redirect()->route('login');
        }
    }

    /**
     * @param Task $task
     * @return RedirectResponse
     */
    public function sync(Task $task)
    {
        if ( Auth::check() ) {
            $asana_client = Client::accessToken(env('ASANA_PERSONAL_ACCESS_TOKEN'));
            $asanaTask = $asana_client->tasks->getTask($task->id);
            // TODO Code to Sync a Task
            dd($asanaTask);
        } else {
            return redirect()->route('login');
        }
    }

    /**
     * @param $asanaTask
     * @return Builder|Builder[]|Collection|Model|RedirectResponse|Task|null
     */
    public function syncAsanaTask($asanaTask)
    {
        if ( Auth::check() ) {
            // TODO: Check to Make sure it doesn't already Exist
            $task = Task::query()->find($asanaTask->gid);
            if ($task instanceof Task !== TRUE) {
                $task = new Task();
                $task->id = $asanaTask->gid;
                $task->name = $asanaTask->name;
                $task->save();
            }

            // Updates?
            $task->name = $asanaTask->name;
            if (isset($asanaTask->completed)) {
                $task->completed = $asanaTask->completed;
            }
            if (isset($asanaTask, $asanaTask->completed_at)) {
                $task->completed_at = $asanaTask->completed_at;
            }
            if (isset($asanaTask->modified_at)) {
                $task->modified_at = $asanaTask->modified_at;
            }

            if (isset($asanaTask->created_at) && $task->created_at !== NULL) {
                // For Mysql Database Support
                $task->created_at = date('Y-m-d H:i:s', strtotime($asanaTask->created_at));
            }

            if (isset($asanaTask->assignee)) {
                if ($asanaTask->assignee !== NULL) {
                    $assignee = Assignee::query()->find($asanaTask->assignee->gid);
                    if ($assignee instanceof Assignee === FALSE) {
                        $assignee = new Assignee();
                        $assignee->id = $asanaTask->assignee->gid;
                        $assignee->name = $asanaTask->assignee->name;
                        $assignee->email = $asanaTask->assignee->email;
                        $assignee->save();
                    }

                    if (!$task->assignee()->find($assignee->id)) {
                        $task->assignee_id = $assignee->id;
                    }
                } else {
                    $task->assignee()->dissociate();
                }
            }

            if ( isset($asanaTask->custom_fields) ) {
                foreach ($asanaTask->custom_fields as $asanaCustomField) {
                    $customField = (new AsanaCustomFieldController())->sync($asanaCustomField);

                    if (!$task->customFields()->find($customField)) {
                        $task->customFields()->attach($customField);
                    }

                    $attributes = [];
                    if (property_exists($asanaCustomField, 'enum_value') && $asanaCustomField->enum_value !== NULL) {
                        $attributes['enum_value'] = $asanaCustomField->enum_value->gid;
                    }
                    if (property_exists($asanaCustomField, 'number_value')) {
                        $attributes['number_value'] = $asanaCustomField->number_value;
                    }
                    if (property_exists($asanaCustomField, 'text_value')) {
                        $attributes['text_value'] = $asanaCustomField->text_value;
                    }

                    if (!empty($attributes)) {
                        $task->customFields()->updateExistingPivot($customField, $attributes);
                    }
                }
            }

//            foreach( $asanaTask->attachments as $asanaAttachment ) {
//                $attachment = Attachment::query()->find($asanaAttachment->gid);
//                if ( $attachment instanceof Attachment === FALSE ) {
//                    $attachment = new Attachment();
//                    $attachment->id = $asanaAttachment->gid;
//                    $attachment->task_id = $task->id;
//                    $attachment->name = $asanaAttachment->name;
//                    $attachment->created_at = $asanaAttachment->created_at;
//                    $attachment->save();
//                }
//
//                if ( !$attachment->storageLocalExists() ) {
////                    dd($asanaAttachment);
//                    $attachment->storageLocalPut(file_get_contents($asanaAttachment->download_url));
//                }
//            }

            if ( isset($asanaTask, $asanaTask->parent) ) {
                if ($asanaTask->parent === NULL) {
                    $task->parent_id = NULL;
                } else {
                    $parentTask = Task::query()->where('id', '=', $asanaTask->parent->gid)->first();
                    if ($parentTask instanceof Task === FALSE) {
                        $parentTask = new Task();
                        $parentTask->id = $asanaTask->parent->gid;
                        $parentTask->name = $asanaTask->parent->name;
                        $parentTask->save();
                    }
                    $task->parent_id = $parentTask->id;
                }
            }

            $task->update();

            return $task;
        } else {
            return redirect()->route('login');
        }
    }

}
