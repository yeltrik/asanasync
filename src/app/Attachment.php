<?php

namespace Yeltrik\AsanaSync\app;

use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Storage;

/**
 * Class Attachment
 * @property string id
 * @property string task_id
 * @property string name
 * @package Yeltrik\AsanaSync\app
 */
class Attachment extends Model
{

    protected $connection = 'asana';
    public $table = 'attachments';

    /**
     * Attachment constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->table = env('DB_DATABASE_ASANA', $this->connection) . '.' . $this->table;
        parent::__construct($attributes);
    }

    public function storageLocalExists()
    {
        return Storage::disk('local')->exists($this->storagePath());
    }

    /**
     * @return string
     * @throws FileNotFoundException
     */
    public function storageLocalGet()
    {
        return Storage::disk('local')->get($this->storagePath());
    }

    /**
     * @param $contents
     */
    public function storageLocalPut($contents)
    {
        Storage::disk('local')->put($this->storagePath(), $contents);
    }

    /**
     * @return string
     */
    public function storagePath()
    {
        return 'asana' . DIRECTORY_SEPARATOR .
            'tasks' . DIRECTORY_SEPARATOR .
            $this->task_id . DIRECTORY_SEPARATOR .
            'attachments' . DIRECTORY_SEPARATOR .
            $this->id . DIRECTORY_SEPARATOR .
            $this->name;
    }

    /**
     * @return HasMany
     */
    public function Participations()
    {
        return $this->hasMany(Participation::class);
    }

    /**
     * @return BelongsTo
     */
    public function Task()
    {
        return $this->belongsTo(Task::class);
    }

}
