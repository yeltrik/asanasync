<?php

namespace Yeltrik\AsanaSync\app;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class EnumOption
 * @property string id
 * @property string name
 * @property bool enabled
 * @package Yeltrik\AsanaSync\app
 */
class EnumOption extends Model
{

    protected $connection = 'asana';
    public $table = 'enum_options';

    /**
     * EnumOption constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->table = env('DB_DATABASE_ASANA', $this->connection) . '.' . $this->table;
        parent::__construct($attributes);
    }

    /**
     * @return BelongsToMany
     */
    public function customFields()
    {
        return $this->belongsToMany(CustomField::class);
    }

}
