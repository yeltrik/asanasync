<?php

namespace Yeltrik\AsanaSync\app;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Task
 * @property string id
 * @property string name
 * @property string completed
 * @property string completed_at
 * @property string modified_at
 * @property string created_at
 * @property string assignee_id
 * @property string parent_id
 * @package Yeltrik\AsanaSync\app
 */
class Task extends Model
{

    protected $connection = 'asana';
    public $table = 'tasks';

    /**
     * Task constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->table = env('DB_DATABASE_ASANA', $this->connection) . '.' . $this->table;
        parent::__construct($attributes);
    }

    /**
     * @return BelongsTo
     */
    public function assignee()
    {
        return $this->belongsTo(Assignee::class);
    }

    /**
     * @return HasMany
     */
    public function attachments()
    {
        return $this->hasMany(Attachment::class);
    }

    /**
     * @return BelongsToMany
     */
    public function customFields()
    {
        return $this->belongsToMany(CustomField::class)->withPivot('enum_value', 'number_value', 'text_value');
    }

    /**
     * @return BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(Task::class);
    }

    /**
     * @return BelongsToMany
     */
    public function projects()
    {
        return $this->belongsToMany(Project::class);
    }

}
