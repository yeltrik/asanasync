<?php

namespace Yeltrik\AsanaSync\app;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class CustomField
 * @property string id
 * @property string name
 * @property string type
 * @package Yeltrik\AsanaSync\app
 */
class CustomField extends Model
{

    protected $connection = 'asana';
    public $table = 'custom_fields';

    /**
     * CustomField constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->table = env('DB_DATABASE_ASANA', $this->connection) . '.' . $this->table;
        parent::__construct($attributes);
    }

    /**
     * @return BelongsToMany
     */
    public function enumOptions()
    {
        return $this->belongsToMany(EnumOption::class);
    }

    /**
     * @return BelongsToMany
     */
    public function tasks()
    {
        return $this->belongsToMany(Task::class);
    }

}
