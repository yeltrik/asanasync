<?php

namespace Yeltrik\AsanaSync\app;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Project
 * @property string id
 * @property string name
 * @package Yeltrik\AsanaSync\app
 */
class Project extends Model
{

    protected $connection = 'asana';
    public $table = 'projects';

    /**
     * Project constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->table = env('DB_DATABASE_ASANA', $this->connection) . '.' . $this->table;
        parent::__construct($attributes);
    }

    /**
     * @return BelongsToMany
     */
    public function customFields()
    {
        return $this->belongsToMany(CustomField::class);
    }

    /**
     * @return BelongsToMany
     */
    public function tasks()
    {
        return $this->belongsToMany(Task::class);
    }

}
