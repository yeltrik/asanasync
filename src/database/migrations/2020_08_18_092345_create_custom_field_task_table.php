<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomFieldTaskTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('asana')->create('custom_field_task', function (Blueprint $table) {
            $table->id();
            $table->string('custom_field_id');
            $table->string('task_id');
            $table->unsignedBigInteger('enum_value')->nullable();
            $table->double('number_value')->nullable();
            $table->string('text_value')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_field_task');
    }
}
