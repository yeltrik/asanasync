@extends('layouts.app')

@section('content')
    <div class="container">

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('welcome') }}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ route('asana.index')  }}">Asana</a></li>
                <li class="breadcrumb-item active" aria-current="page">Assignees</li>
            </ol>
        </nav>

        <h1>
            Assignees
        </h1>

        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
            </tr>
            </thead>
            <tbody>

            @foreach($assignees as $assignee)
                <tr>
                    <th scope="row">
                        <a href="{{ route('asana.assignees.show', $assignee) }}" class="text-dark">
                            {{ $assignee->name }}
                        </a>
                    </th>
                    <td>
                        {{ $assignee->email }}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </div>
@endsection
