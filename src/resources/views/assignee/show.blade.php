@extends('layouts.app')

@section('content')
    <div class="container">

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('welcome') }}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ route('asana.index')  }}">Asana</a></li>
                <li class="breadcrumb-item"><a href="{{ route('asana.assignees.index')  }}">Assignees</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{ $assignee->name }}</li>
            </ol>
        </nav>

        <h1>
            {{ $assignee->name }}
        </h1>

        <h2>
            {{ $assignee->email }}
        </h2>

        <h3>
            Tasks
        </h3>

        @foreach($assignee->tasks as $task)
            <div>
                <a href="{{ route('asana.tasks.show', $task) }}" class="text-dark">
                    {{ $task->name }}
                </a>
            </div>
        @endforeach

    </div>
@endsection
