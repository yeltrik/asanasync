@extends('layouts.app')

@section('content')
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('welcome') }}">Asana</a></li>
                <li class="breadcrumb-item">
                    @if($task->projects->first())
                        @if($task->projects->count() == 1)
                            <a href="{{ route('asana.projects.show', $task->projects->first()->id) }}">{{ $task->projects->first()->name }}</a>
                        @else
                            @foreach($task->projects as $project)
                                <a href="{{ route('asana.projects.show', $project->id) }}"
                                   class="mr-1">({{ $project->name }})</a>
                            @endforeach
                        @endif
                    @else
                        {{--Unknown Project--}}
                    @endif
                </li>
                <li class="breadcrumb-item active" aria-current="page">{{ $task->name }}</li>

            </ol>
        </nav>

        <div class="d-flex justify-content-between">
            <h1>
                {{ $task->name }}
            </h1>
            <div class="d-flex">
                @if($task->projects->first())
                    <a href="https://app.asana.com/0/{{ $task->projects()->first()->id }}/{{ $task->id }}/f"
                       target="asana_{{ $task->id }}" class="btn btn-info ml-1">
                        <i class="far fa-eye"></i> View in Asana
                    </a>
                @endif
                <a href="{{ route('asana.tasks.sync', $task) }}">
                    <button class="btn btn-primary">
                        <i class="fas fa-sync"></i> Sync Task
                    </button>
                </a>
            </div>
        </div>

        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">Attribute</th>
                <th scope="col">Value</th>
            </tr>
            </thead>
            <tbody>

            @if($task->parent)
                <tr>
                    <th>
                        Parent
                    </th>
                    <td>
                        <a href="{{ route('asana.tasks.show', $task->parent) }}" class="text-dark">
                            {{ $task->parent->name }}
                        </a>
                    </td>
                </tr>
            @endif

            @foreach($task->customFields as $customField)
                <tr>
                    <th scope="row">{{ $customField->name }}</th>
                    <td>
                        <h5>
                            {{ $customField->pivot->text_value }}
                            {{ $customField->pivot->number_value }}
                            @if( $customField->pivot->enum_value !== NULL )
                                {{ \Yeltrik\AsanaSync\app\EnumOption::query()->where('id', '=', $customField->pivot->enum_value)->first()->name }}
                            @endif
                        </h5>
                    </td>
                </tr>
            @endforeach
            <tr>
                <th>
                    Completed
                </th>
                <td>
                    @if($task->completed)
                        <button class="btn btn-success">
                            <i class="fas fa-check"></i>
                        </button>
                    @else
                        &nbsp;
                    @endif
                </td>
            </tr>
            <tr>
                <th>
                    Assignee
                </th>
                <td>
                    @if($task->assignee)
                        <a href="{{ route('asana.assignees.show', $task->assignee) }}">
                            {{  $task->assignee->name }}
                        </a>
                    @else
                        &nbsp;
                    @endif
                </td>
            </tr>
            <tr>
                <th>
                    Completed At
                </th>
                <td>
                    {{ $task->completed_at }}
                </td>
            </tr>

            <tr>
                <th>
                    Attachments
                </th>
                <td>
                    @foreach($task->attachments as $attachment)
                        <div>
                            <a href="#">
                                <i class="fa fa-file-alt"></i>
                                {{ $attachment->name }}
                            </a>
                        </div>
                    @endforeach
                </td>
            </tr>

            </tbody>
        </table>

    </div>
@endsection
