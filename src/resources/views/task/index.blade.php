@extends('layouts.app')

@section('content')
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('welcome') }}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ route('asana.index')  }}">Asana</a></li>
                <li class="breadcrumb-item active" aria-current="page">Tasks</li>
            </ol>
        </nav>

        @if($tasks->count() > 0)
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">Project Name</th>
                    <th scope="col">Parent Project(s)</th>
                    <th scope="col">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($tasks as $task)
                    <tr>
                        <th scope="row">
                            <a href="{{ route('asana.tasks.show', $task) }}">
                                {{ $task->name ? $task->name : '{Unnamed}' }}
                            </a>
                        </th>
                        <td>
                            @foreach($task->projects as $project)
                                <a href="{{ route('asana.projects.show', $project) }}">
                                    <span class="badge badge-pill badge-info">
                                        {{ $project->name }}
                                    </span>
                                </a>
                            @endforeach
                        </td>
                        <td>

                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
            {{ $tasks->links() }}
        @else
            <h3>No Tasks have been added</h3>
        @endif
    </div>
@endsection
