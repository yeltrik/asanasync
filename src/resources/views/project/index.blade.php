@extends('layouts.app')

@section('content')
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('welcome') }}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ route('asana.index')  }}">Asana</a></li>
                <li class="breadcrumb-item active" aria-current="page">Projects</li>
            </ol>
        </nav>

        <a href="{{ route('asana.projects.create') }}" class="btn btn-primary float-right">
            <i class="far fa-plus-square"></i> Add Project
        </a>

        @if($projects->count() > 0)
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">Project Name</th>
                    <th scope="col">Task count</th>
                    <th scope="col">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($projects as $project)
                    <tr>
                        <th scope="row">
                            <a href="{{ route('asana.projects.show', $project) }}">
                                {{ $project->name ? $project->name : '{Unnamed}' }}
                            </a>
                        </th>
                        <td>
                            <span class="badge badge-pill badge-info">
                                {{ $project->tasks()->count() }}
                            </span>
                        </td>
                        <td>
                            <a href="https://app.asana.com/0/{{ $project->id }}/list" target="asana_{{ $project->id }}" class="btn btn-info ml-1">
                                <i class="far fa-eye"></i> View in Asana
                            </a>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        @else
            <h3>No Projects have been added</h3>
        @endif
    </div>
@endsection
