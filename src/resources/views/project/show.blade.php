@extends('layouts.app')

@section('content')
<div class="">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('welcome') }}">Asana</a></li>
                <li class="breadcrumb-item"><a href="{{ route('asana.projects.index') }}">Projects</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{ $project->name ? $project->name : '{Unnamed}' }}</li>
            </ol>
        </nav>
    </div>


    <div class="d-flex float-right">
        <a href="https://app.asana.com/0/{{ $project->id }}/list" target="asana_{{ $project->id }}" class="btn btn-info ml-1">
            <i class="far fa-eye"></i> View in Asana
        </a>

        <div class="dropdown">
            <button class="btn btn-outline-primary dropdown-toggle ml-1" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Asana Sync
            </button>
            <div class="dropdown-menu dropdown-menu-right" style="z-index: 1000;" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="{{ route('asana.projects.name-note.sync', $project) }}">Project (Name and Note)</a>
                <a class="dropdown-item" href="{{ route('asana.projects.custom-fields.sync', $project) }}">Project (Custom  Fields)</a>
                <a class="dropdown-item" href="{{ route('asana.projects.tasks.sync', $project) }}">Project (Tasks)</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="{{ route('asana.projects.tasks.modified.sync', [
                                        'project' => $project,
                                    ]) }}">Tasks (Updated Recently)</a>
                <a class="dropdown-item" href="{{ route('asana.projects.tasks.parent.sync', $project) }}">Tasks (Parents)</a>
                <a class="dropdown-item" href="{{ route('asana.projects.tasks.assignees.sync', $project) }}">Tasks (Assignees)</a>
                <a class="dropdown-item" href="{{ route('asana.projects.tasks.custom-fields.sync', $project) }}">Tasks (Custom Fields)</a>
                <a class="dropdown-item disabled" href="#">Tasks (Attachments)</a>
                <a class="dropdown-item disabled" href="#">Tasks (Deleted)</a>
            </div>
        </div>
    </div>
    <div style="clear: both;"></div>

    <div class="accordion" id="accordion">
        <div class="card">
            <div class="card-header" id="headingMain">
                <h2 class="mb-0">
                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseMain" aria-expanded="false" aria-controls="collapseMain">
                        <div class="d-flex justify-content-between">
                            <h1 class="text-dark">
                                {{ $project->name ? $project->name : "{Unnamed}" }}
                            </h1>
                        </div>
                    </button>
                </h2>

            </div>
            <div id="collapseMain" class="collapse" aria-labelledby="headingMain" data-parent="#accordion">
                <div class="card-body">
                    {{ $project->notes }}
                </div>
            </div>

            @if( $project->tasks->count() > 0 )
            <div class="card">
                <div class="card-header" id="headingTwo">
                    <h2 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTasks" aria-expanded="true" aria-controls="collapseTasks">
                            Tasks
                            <span class="badge badge-pill badge-info ml-2">
                                {{ $project->tasks()->count() }}
                            </span>
                        </button>
                    </h2>
                </div>
                <div id="collapseTasks" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordion">
                    <div class="card-body">
{{--                        @include('project.tasks')--}}
{{--                        @yield('table')--}}

                        @if( $project->tasks->count() > 0 )
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">Name</th>
                                    <th scope="col">Parent</th>
                                    <th scope="col">Assignee</th>
                                    <th scope="col">Completed</th>
                                    <th scope="col">Attachments</th>
                                    @foreach($project->customFields as $customField)
                                        <th scope="col">
                                            {{ $customField->name }}
                                        </th>
                                    @endforeach
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($tasks as $task)
                                    <tr>
                                        <th scope="row">
                                            <a href="{{ route('asana.tasks.show', $task) }}">
                                                {{ $task->name }}
                                            </a>
                                            <a href="https://app.asana.com/0/{{ $project->id }}/list" target="asana_{{ $project->id }}" class="btn btn-info ml-1 float-right">
                                                <i class="far fa-eye"></i> View in Asana
                                            </a>
                                        </th>
                                        <td>
                                            @if($task->parent)
                                                <a href="{{ route('asana.tasks.show', $task->parent) }}">
                                                    {{ $task->parent->name }}
                                                </a>
                                            @else
                                                &nbsp;
                                            @endif
                                        </td>
                                        <td>
                                            @if($task->assignee)
                                                <a href="{{ route('asana.assignees.show', $task->assignee) }}">
                                                    {{ $task->assignee->name }}
                                                </a>
                                            @else
                                                &nbsp;
                                            @endif
                                        </td>
                                        <td>
                                            @if($task->completed)
                                                <button class="btn btn-success">
                                                    <i class="fas fa-check"></i>
                                                    {{ $task->completed_at }}
                                                </button>
                                            @else
                                                &nbsp;
                                            @endif
                                        </td>
                                        <td>
{{--                                            {{ $task->attachments->count() }}--}}
                                            @foreach($task->attachments as $attachment)
                                                <div>
                                                    <a href="#">
                                                        <i class="fa fa-file-alt"></i>
                                                        {{ $attachment->name }}
                                                    </a>
                                                </div>
                                            @endforeach
                                        </td>
                                        @foreach($project->customFields as $customField)
                                            @if(!$task->customFields->find($customField))
                                                <td scope="col" class="disabled">
                                                    needs sync...
                                                </td>
                                            @else
                                                <td scope="col">
                                                    @if($task->customFields->find($customField)->pivot->enum_value !== NULL)
                                                        @if(\Yeltrik\AsanaSync\app\EnumOption::query()->find($task->customFields->find($customField)->pivot->enum_value) !== NULL)
                                                            {{ \Yeltrik\AsanaSync\app\EnumOption::query()->find($task->customFields->find($customField)->pivot->enum_value)->name }}
                                                        @else
                                                            (ERROR: Sync)
                                                        @endif
                                                    @endif
                                                    {{ $task->customFields->find($customField)->pivot->number_value }}
                                                    {{ $task->customFields->find($customField)->pivot->text_value }}
                                                </td>
                                            @endif
                                        @endforeach
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{ $tasks->links() }}
                        @endif
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>

</div>
@endsection
