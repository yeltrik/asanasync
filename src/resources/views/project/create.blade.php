@extends('layouts.app')

@section('content')
<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('welcome') }}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{ route('asana.index')  }}">Asana</a></li>
            <li class="breadcrumb-item"><a href="{{ route('asana.projects.index') }}">Projects</a></li>
            <li class="breadcrumb-item active" aria-current="page">Create</li>
        </ol>
    </nav>

    <h1>
        Sync New Asana Project
    </h1>

{{--    @if ($errors->any())--}}
{{--        <div class="alert alert-danger">--}}
{{--            @foreach ($errors->all() as $error)--}}
{{--                {{ $error }}<br>--}}
{{--            @endforeach--}}
{{--        </div>--}}
{{--    @endif--}}

    <form method="POST" action="{{ route('asana.projects.store') }}" novalidate>
        @csrf

        @error('id')
            <p class="alert alert-danger">{{ $message }}</p>
        @enderror
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Asana Gid</span>
            </div>
            <input type="text" name="id" value="{{ old('id') }}" class="form-control" placeholder="1234567890123456" aria-label="Asana GID" aria-describedby="basic-addon1">
        </div>

        @error('name')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon2">Project Name</span>
            </div>
            <input type="text" name="name" value="{{ old('name') }}" class="form-control" placeholder="ABC XYZ" aria-label="Project Name" aria-describedby="basic-addon2">
        </div>

        <button type="submit" class="btn btn-success btn-lg btn-block"><i class="fas fa-sync"></i> Sync Asana Project</button>

    </form>


</div>
@endsection
