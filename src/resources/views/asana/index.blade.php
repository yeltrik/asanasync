@extends('layouts.app')

@section('content')
    <div class="container">

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('welcome') }}">Home</a></li>
                <li class="breadcrumb-item active">Asana</li>
            </ol>
        </nav>


        <ul class="list-group">
            <li class="list-group-item">
                <a href="{{ route('asana.assignees.index') }}">
                    Assignees
                </a>
            </li>
            <li class="list-group-item">
                <a href="{{ route('asana.attachments.index') }}">
                    Attachment
                </a>
            </li>
            <li class="list-group-item">
                <a href="{{ route('asana.projects.index') }}">
                    Project
                </a>
            </li>
            <li class="list-group-item">
                <a href="{{ route('asana.tasks.index') }}">
                    Task
                </a>
            </li>
        </ul>

    </div>
@endsection
