@extends('layouts.app')

@section('content')
<div class="">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('welcome') }}">Asana</a></li>
            <li class="breadcrumb-item"><a href="#">Attachments</a></li>
{{--            <li class="breadcrumb-item"><a href="{{ route('asana.tasks.show', $attachment->task()->get()) }}">{{ $attachment->task()->get()->name }}</a></li>--}}
            <li class="breadcrumb-item active" aria-current="page">{{ $attachment->name }}</li>
        </ol>
    </nav>

    <h2>
        <a href="{{ route('asana.tasks.show', $attachment->task) }}" class="text-dark">
            {{ $attachment->task->name }}
        </a>
    </h2>

    <h1>
        {{ $attachment->name }}
    </h1>

    <div>
        Created: {{ $attachment->created_at }}
    </div>

    <div>
        Updated: {{ $attachment->updated_at }}
    </div>

{{--    <div class="d-flex float-right">--}}

{{--        @if( $attachment->storageLocalExists() )--}}
{{--            <a href="#" class="btn btn-info">--}}
{{--                Download From Local Storage--}}
{{--            </a>--}}
{{--        @endif--}}

{{--        <a href="#" class="btn btn-info">--}}
{{--            View in Asana--}}
{{--        </a>--}}

{{--        <a href="{{ route('asana.attachments.participations.sync', $attachment) }}">--}}
{{--            <button class="btn btn-primary">--}}
{{--                <i class="fas fa-sync"></i> Participation--}}
{{--            </button>--}}
{{--        </a>--}}
{{--    </div>--}}

{{--    --}}
{{--    <h1>--}}
{{--        Participation--}}
{{--    </h1>--}}

{{--    @foreach($attachment->participations as $participation)--}}
{{--        <div>--}}
{{--            {{ $participation->task->name }}--}}
{{--        </div>--}}
{{--    @endforeach--}}

</div>
@endsection
