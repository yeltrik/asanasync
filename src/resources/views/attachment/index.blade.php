@extends('layouts.app')

@section('content')
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('welcome') }}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ route('asana.index') }}">Asana</a></li>
                <li class="breadcrumb-item active">Attachments</li>
            </ol>
        </nav>

        <h1>
            Attachments
        </h1>

        @if( $attachments->count() > 0)
            <table class="table table-striped">
                <thead>
                <th scope="col">Name</th>
                <th scope="col">Parent Task</th>
                <th scope="col">Created</th>
                <th scope="col">Updated</th>
                </thead>
                <tbody>
                @foreach($attachments as $attachment)
                    <tr>
                        <th scope="row">
                            <a href="{{ route('asana.attachments.show', $attachment) }}" class="text-dark">
                                {{ $attachment->name }}
                            </a>
                        </th>
                        <td>
                            <a href="{{ route('asana.tasks.show', $attachment->task) }}" class="text-dark">
                                {{ $attachment->task->name }}
                            </a>
                        </td>
                        <td>{{ $attachment->created_at }}</td>
                        <td>{{ $attachment->updated_at }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>


            <div class="container">
                {{ $attachments->links() }}
            </div>
        @else
            <h3>There are no attachments</h3>
        @endif

    </div>
@endsection
